export CLICOLOR=true
export LSCOLORS=GxFxCxDxBxegedabagaced

alias la='ls -aGfh'

# Prompt
[[ -f "$HOME/.bash_prompt" ]] && source "$HOME/.bash_prompt"

# virtualenv setting
export WORKON_HOME=~/Envs
export PROJECT_HOME=~/Workspace
source /usr/local/bin/virtualenvwrapper.sh

# Lunchy auto-
LUNCHY_DIR=$(dirname `gem which lunchy`)/../extras
if [ -f $LUNCHY_DIR/lunchy-completion.bash ]; then
	. $LUNCHY_DIR/lunchy-completion.bash
fi
